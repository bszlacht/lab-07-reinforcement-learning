# Lab 3. Introduction to Reinforcement Learning

## TODO:

Fill missing code according to the `TODO:` comments in the following files:
- `src/sandbox/algorithms/q_learning/qlearning.py`
- `src/sandbox/algorithms/q_learning/double_q.py`
- `src/sandbox/algorithms/q_learning/dynaq.py`
- `src/sandbox/algorithms/q_learning/dynaq_plus.py`
- `src/sandbox/environments/grid_pathfinding/env.py`

## Grading

* [ ] Make sure, you have a **private** group
  * [how to create a group](https://docs.gitlab.com/ee/user/group/#create-a-group)
* [ ] Fork this project into your private group
  * [how to create a fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)
* [ ] Add @bobot-is-a-bot as the new project's member (role: **maintainer**)
  * [how to add an user](https://docs.gitlab.com/ee/user/project/members/index.html#add-a-user)

## How To Submit Solutions

* [ ] Clone repository: git clone:
    ```bash
    git clone <repository url>
    ```
* [ ] Solve the exercises
    * use MiniZincIDE, whatever
* [ ] Commit your changes
    ```bash
    git add <path to the changed files>
    git commit -m <commit message>
    ```
* [ ] Push changes to the gitlab master branch
    ```bash
    git push -u origin master
    ```

The rest will be taken care of automatically. You can check the `GRADE.md` file for your grade / test results. 
Be aware that it may take some time (up to one hour) till this file

## How To Benchmark

The most crude and basic way:

```bash
pip install -r requirements.txt
PYTHONPATH=src python src/benchmarks/1-bandits.py
```
